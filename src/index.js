import React from 'react'
import { render } from 'react-dom'
import Tree from './Tree'

render(<Tree />, document.getElementById('root'))

// https://api.myjson.com/bins/f27wf
