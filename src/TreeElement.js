import React from 'react'

function TreeElement({
  name,
  id,
  indent,
  editHandler,
  addNextHandler,
  addChildHandler,
  removeHandler
}) {
  const makeIndent = ind => {
    if (ind === 0) return '├'
    return '    │'.repeat(ind - 1) + '    ├'
  }

  const stringOfIndent = makeIndent(indent)
  const normalizedName = name || 'Noname'

  return (
    <pre className="element">
      {stringOfIndent + ' '}
      <span className="name" onClick={editHandler}>
        {normalizedName}
      </span>{' '}
      <span className="id">(#{id})</span>{' '}
      <button
        className="control"
        style={{ color: 'orange' }}
        onClick={addChildHandler}
      >
        >
      </button>{' '}
      <button
        className="control"
        style={{ color: 'green' }}
        onClick={addNextHandler}
      >
        +
      </button>{' '}
      <button
        className="control"
        style={{ color: 'red' }}
        onClick={removeHandler}
      >
        -
      </button>
    </pre>
  )
}

export default TreeElement
