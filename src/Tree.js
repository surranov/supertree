import React, { Component } from 'react'
import TreeElement from './TreeElement'
import fetchElements from './api'

class Tree extends Component {
  state = {
    elementsData: {
      pointers: [],
      elements: {}
    }
  }

  componentWillMount() {
    const fromLocalStorage = localStorage.getItem('superTree')
    if (fromLocalStorage) {
      const elementsData = JSON.parse(fromLocalStorage)
      this.setState({
        elementsData
      })
    } else {
      this.fetchFromApi()
    }
  }

  componentDidUpdate() {
    this.updateLocalStorage()
  }

  makeTree(curentSet, parentId, indent = 0) {
    return curentSet.map(id => {
      const el = this.state.elementsData.elements[id]
      const hasChilds = !!el.childs
      let childs

      if (hasChilds) {
        childs = this.makeTree(el.childs, id, indent + 1)
      }

      return (
        <div key={id}>
          <TreeElement
            name={el.name}
            id={id}
            indent={indent}
            editHandler={this.editHandler.bind(this, id)}
            addNextHandler={this.addNextHandler.bind(this, id, parentId)}
            addChildHandler={this.addChildHandler.bind(this, id)}
            removeHandler={this.removeHandler.bind(this, id, parentId)}
          />
          {childs}
        </div>
      )
    })
  }

  render() {
    return (
      <div>
        {this.makeTree(this.state.elementsData.pointers, null)}
        <button className="reset" onClick={this.resetLocalStorage.bind(this)}>
          Reset tree
        </button>
      </div>
    )
  }

  editHandler(clickedId) {
    let { elements, pointers } = this.state.elementsData
    let newName = prompt(
      `Edit element #${clickedId} name`,
      elements[clickedId].name
    )
    if (!newName) return

    elements = Object.assign({}, elements)

    elements[clickedId].name = newName.trim()

    this.setState({
      elementsData: {
        elements,
        pointers
      }
    })
  }

  addNextHandler(clickedId, parentId) {
    let newName = prompt('Enter new element name')
    if (!newName) return

    let { elements, pointers } = this.state.elementsData
    elements = Object.assign({}, elements)
    const newId = Math.max(...Object.keys(elements)) + 1

    if (parentId) {
      let childs = elements[parentId].childs
      childs.splice(childs.indexOf(clickedId) + 1, 0, newId)
    } else {
      pointers = pointers.slice()
      pointers.splice(pointers.indexOf(clickedId) + 1, 0, newId)
    }

    elements[newId] = {
      name: `${newName.trim()}`,
      childs: []
    }

    this.setState({
      elementsData: {
        elements,
        pointers
      }
    })
  }

  addChildHandler(clickedId) {
    let newName = prompt('Enter new element name')
    if (!newName) return

    let { elements, pointers } = this.state.elementsData
    elements = Object.assign({}, elements)
    let childs = elements[clickedId].childs
    let newId = Math.max(...Object.keys(elements)) + 1

    childs.unshift(newId)

    elements[newId] = {
      name: `${newName.trim()}`,
      childs: []
    }

    this.setState({
      elementsData: {
        elements,
        pointers
      }
    })
  }

  removeHandler(clickedId, parentId) {
    let { elements, pointers } = this.state.elementsData
    elements = Object.assign({}, elements)

    if (parentId) {
      elements[parentId].childs = elements[parentId].childs.filter(
        el => el !== clickedId
      )
    } else {
      pointers = pointers.filter(el => el !== clickedId)
    }

    delete elements[clickedId]

    this.setState({
      elementsData: {
        elements,
        pointers
      }
    })
  }

  fetchFromApi() {
      fetchElements().then(response => {
      const { pointers, elements } = response
      this.setState({
        elementsData: {
          pointers,
          elements
        }
      })
    })
  }

  updateLocalStorage() {
    localStorage.setItem('superTree', JSON.stringify(this.state.elementsData))
  }

  resetLocalStorage() {
    localStorage.removeItem('superTree')
    this.fetchFromApi()
  }
}

export default Tree
